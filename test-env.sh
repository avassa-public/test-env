#!/usr/bin/env sh
progress() {
    echo "$@" >&2
}
error() {
    echo "$@" >&2
    exit 1
}
basedir="$(cd "$(dirname "$0")" && pwd)"
baseimagedir="${basedir}/base"
imagedir="${basedir}/images"
domaindir="${basedir}/domains"
base_domaindir="${domaindir}"
netdir="${basedir}/network"
sshdir="${basedir}/ssh"
tmpdir="${basedir}/prepare"
location_cache="${basedir}/.location-cache.json"
: "${LIBVIRT_DEFAULT_URI:=qemu:///system}"
export LIBVIRT_DEFAULT_URI
name() {
    printf "${hostname_prefix}%02d" "$1"
}
has() {
    command -v "$@" >/dev/null 2>/dev/null
}
check_cmds() {
    for cmd in "$@"; do
        if ! has "${cmd}"; then
            error "${cmd} not found, required to run this script"
        fi
    done
}

foreach_domain() {
    _callback="${1:?}"
    if [ -d "${domaindir}" ]; then
        for domain in "${domaindir}"/*.xml "${domaindir}"/*/*.xml; do
            if [ -f "${domain}" ]; then
                domain=${domain##"$base_domaindir"/}
                domain=${domain%.xml}
                $_callback "${domain%/*}-${domain#*/}"
            fi
        done
    fi
}
foreach_network() {
    _callback="${1:?}"
    if [ -d "${netdir}" ]; then
        for network in "${netdir}"/*.xml "${netdir}"/*/*.xml; do
            if [ -f "${network}" ]; then
                network=${network##*/}
                network=${network%.xml}
                $_callback "${network}"
            fi
        done
    fi
}

case "$1" in
    destroy)
        shift
        delete=
        site_name=
        while [ $# -ge 1 ]; do
            case "$1" in
                --target-env) shift; target_env=$1;;
                --delete) delete=yes;; # handled above
                *)
                    if [ -z "${site_name}" ]; then
                        site_name=$1
                        imagedir="${imagedir}/${site_name}"
                        domaindir="${domaindir}/${site_name}"
                        netdir="${netdir}/${site_name}"
                        sshdir="${sshdir}/${site_name}"
                        tmpdir="${tmpdir}/${site_name}"
                    else
                        error "unknown option: $1"
                    fi
            esac
            shift
        done
        : "${target_env:=${TEST_ENV_TARGET}}"
        if [ -n "${delete}" ]; then
            lazy_check_supctl() {
                check_cmds supctl
                environment=$(supctl profile show | sed -En 's/Host: (api.)?//p')
                if [ -n "${target_env}" ]; then
                    if [ "${target_env}" != "${environment}" ]; then
                        error "$(cat <<EOF
aborting due to discrepancy
    - supctl is logged into: ${environment},
    - target environment: ${target_env}
EOF
)"
                    fi
                    lazy_check_supctl() {
                        :
                    }
                elif ! tty >/dev/null; then
                    error 'Aborting: --target-env must be set when running non-interactively'
                else
                    lazy_check_supctl() {
                        printf "Delete site %s in environment %s, please confirm" "${site_name}" "${environment}"
                        read -r _continue || error "\naborting"
                    }
                    lazy_check_supctl
                fi
            }
            if [ -n "${site_name}" ]; then
                lazy_check_supctl
                progress "Deleting site ${site_name} from environment ${environment}"
                supctl delete system sites "${site_name}"
            else
                for sitedir in "${domaindir}"/*; do
                    site_name=$(basename "${sitedir}")
                    if [ "${site_name}" != '*' ]; then
                        lazy_check_supctl
                        progress "Deleting site ${site_name} from environment ${environment}"
                        supctl delete system sites "${site_name}"
                    fi
                done
            fi
        fi
        # shellcheck disable=SC2317
        destroy_persistent_domain() {
            virsh destroy "${1:?}" 2>/dev/null
            virsh undefine --nvram "$1"
        }
        foreach_domain destroy_persistent_domain
        # shellcheck disable=SC2317
        destroy_persistent_network() {
            virsh net-destroy "${1:?}" 2>/dev/null
            virsh net-undefine "$1"
        }
        foreach_network destroy_persistent_network
        rm -rf "${imagedir}" "${domaindir}" "${netdir}" "${sshdir}" "${tmpdir}"
        exit 0
        ;;
    start)
        if [ -n "${2}" ]; then
            site_name="${2}"
            domaindir="${domaindir}/${site_name}"
            netdir="${netdir}/${site_name}"
        fi
        # shellcheck disable=SC2317
        start_network() {
            virsh net-start "${1?:}"
        }
        foreach_network start_network
        # shellcheck disable=SC2317
        start_domain() {
            virsh start "${1?:}"
        }
        foreach_domain start_domain
        exit 0
        ;;
    stop)
        if [ -n "${2}" ]; then
            site_name="${2}"
            domaindir="${domaindir}/${site_name}"
            netdir="${netdir}/${site_name}"
        fi
        # shellcheck disable=SC2317
        stop_domain() {
            virsh destroy "${1?:}"
        }
        foreach_domain stop_domain
        # shellcheck disable=SC2317
        stop_network() {
            virsh net-destroy "${1?:}"
        }
        foreach_network stop_network
        exit 0
        ;;
    cache)
        cache_base_image_only=yes
        shift
        ;;
    create)
        shift
        ;;
    *)
        echo "Usage:" >&2
        echo >&2
        echo "$0 create" >&2
        echo "    --num-hosts <n>" >&2
        echo "    --site-name <name>" >&2
        echo "    --site-label <key or key=value>" >&2
        echo "    --network <X.Y.Z.0>" >&2
        echo "    --cpu <vcpu>" >&2
        echo "    --memory <N>G" >&2
        echo "    --disk-size <N>G" >&2
        echo "    --hostname-prefix <prefix>" >&2
        echo "    --no-install" >&2
        echo "    --target-env <env.company.avassa.net> (default \$TEST_ENV_TARGET)" >&2
        echo "    --distro <debian-XX|ubuntu-XX.XX|coreos|rocky-9>" >&2
        echo "    --revision <rev>" >&2
        echo "    --base-image <image file>" >&2
        echo "    --location <address>" >&2
        echo "    --arch <x86_64|aarch64>" >&2
        echo "    --profile <config-profile>" >&2
        echo "    -- [flags passed to the supd install script]" >&2
        echo >&2
        echo "$0 cache" >&2
        echo "    --distro <debian-XX|ubuntu-XX.XX|coreos|rocky-9>" >&2
        echo "    --revision <rev>" >&2
        echo "    --arch <x86_64|aarch64>" >&2
        echo "    --profile <config-profile>" >&2
        echo >&2
        echo "$0 start [site-name]" >&2
        echo "$0 stop [site-name]" >&2
        echo "$0 destroy [site-name] [--delete [--target-env <env.company.avassa.net>]]" >&2
        exit 1
esac

prevparam=
for param in "$@"; do
    if [ "$prevparam" = "--profile" ]; then
        # shellcheck disable=SC2034 # profile variable is used in the sourced .cfg file below
        profile="$param"
        break
    fi
    prevparam="$param"
done

if [ -f "${basedir}/test-env.cfg" ]; then
    # shellcheck source=/dev/null
    . "${basedir}/test-env.cfg"
fi

: "${site_labels:=}"
while [ $# -ge 1 ]; do
    case "$1" in
        --num-hosts) shift; num_hosts=$1;;
        --site-name) shift; site_name=$1;;
        --site-label) shift; site_labels="${site_labels}$(printf '\t')$1";;
        --network) shift; network=$1;;
        --cpu) shift; cpu=$1;;
        --memory) shift; memory=$1;;
        --disk-size) shift; disk_size=$1;;
        --hostname-prefix) shift; hostname_prefix=$1;;
        --no-install) no_install=yes;;
        --target-env) shift; target_env=$1;;
        --distro) shift; distro=$1;;
        --revision) shift; revision=$1;;
        --base-image) shift; base_image=$1;;
        --location) shift; location=$1;;
        --arch) shift; arch=$1;;
        --profile) shift;; # handled above
        --) shift; installer_flags="$*"; break;;
        *) error "unknown option: \"$1\""
    esac
    shift
done

: "${arch:=$(uname -m)}"
case "${arch}" in
    x86_64|aarch64) :;;
    *) error "Unsupported target architecture: ${arch}"
esac

check_cmds curl sed grep

: "${distro:=debian-11}"

mkdir -p "${baseimagedir}"
if [ -n "${base_image}" ]; then
    baseimagefile=${base_image}
    explicit_base_image=explicit
fi

compression=
case "${explicit_base_image}/${distro}" in
    /debian-*)
        : "${revision:=latest}"
        baseurl="https://cloud.debian.org/images/cloud"
        case "${distro}" in
            debian-11) baseurl="${baseurl}/bullseye/${revision}";;
            debian-12) baseurl="${baseurl}/bookworm/${revision}";;
            *) error "Unsupported major version: ${distro}"
        esac
        case "${arch}" in
            x86_64) urlarch=amd64;;
            aarch64) urlarch=arm64
        esac
        case "${revision}" in
            latest) baseimagefile="${distro}-generic-${urlarch}.qcow2" ;;
            *)      baseimagefile="${distro}-generic-${urlarch}-${revision}.qcow2"
        esac
        imageurl="${baseurl}/${baseimagefile}"
        metadatafile=".${distro}.${revision}.SHA512SUMS"
        metadatafilepath="${baseimagedir}/${metadatafile}"
        metadataurl="${baseurl}/SHA512SUMS"
        check_cmds sha512sum
        verify_download() {
            grep -F "${baseimagefile}" "${metadatafilepath}"\
                | (cd "${baseimagedir}" && sha512sum -c)
        }
        ;;
    /ubuntu-*)
        : "${revision:=latest}"
        baseurl="https://cloud-images.ubuntu.com/releases"
        case "${revision}" in
            release*) baseurl="${baseurl}/${distro#*-}/${revision}";;
            latest)   baseurl="${baseurl}/${distro#*-}/release";;
            *)        baseurl="${baseurl}/${distro#*-}/release-${revision}";;
        esac
        case "${arch}" in
            x86_64) urlarch=amd64;;
            aarch64) urlarch=arm64
        esac
        urlbaseimagefile="${distro}-server-cloudimg-${urlarch}.img"
        imageurl="${baseurl}/${urlbaseimagefile}"
        baseimagefile="${distro}-server-cloudimg-${urlarch}-${revision}.qcow2"
        metadatafile=".${distro}.${revision#release-}.SHA256SUMS"
        metadatafilepath="${baseimagedir}/${metadatafile}"
        metadataurl="${baseurl}/SHA256SUMS"
        check_cmds sha256sum
        verify_download() {
            grep -F "${urlbaseimagefile}" "${metadatafilepath}" \
                | sed 's#'"${urlbaseimagefile}"'#'"${baseimagefile}"'#' \
                | (cd "${baseimagedir}" && sha256sum -c)
        }
        ;;
    /coreos)
        check_cmds jq
        : "${revision:=latest}"
        baseurl=https://builds.coreos.fedoraproject.org
        metadatafile=".${distro}.${revision}.json"
        metadatafilepath="${baseimagedir}/${metadatafile}"
        if [ "${revision}" = latest ]; then
            if [ ! -f "${metadatafilepath}" ]; then
                url=${baseurl}/streams/stable.json
                if ! curl -Lfso "${metadatafilepath}" "${url}"; then
                    error "Downloading metadata file failed from ${url}"
                fi
            fi
            revision=$(jq -r .architectures."${arch}".artifacts.qemu.release \
                       < "${metadatafilepath}")
            if [ -z "${revision}" ]; then
                error "Could not determine the latest coreos revision" \
                      "from ${metadatafilepath}"
            fi
            metadatafile=".${distro}.${revision}.json"
            metadatafilepath="${baseimagedir}/${metadatafile}"
        fi
        revbaseurl=${baseurl}/prod/streams/stable/builds/${revision}/${arch}
        metadataurl=${revbaseurl}/meta.json
        process_metadata() {
            baseimagefile=$(jq -r .images.qemu.path < "${metadatafilepath}")
            imageurl="${revbaseurl}/${baseimagefile}"
            case "${baseimagefile}" in
                *.xz)
                    baseimagefile=${baseimagefile%.xz}
                    compression=.xz
                    decompress="xz -d"
            esac
        }
        check_cmds sha256sum
        verify_download() {
            printf "%s %s" \
                "$(jq -r '.images.qemu."uncompressed-sha256"' \
                       < "${metadatafilepath}")" \
                "${baseimagedir}/${baseimagefile}" \
                | sha256sum -c
        }
        ;;
    /rocky-*)
        case "${distro}" in
            rocky-9) :;;
            *) error "Unsupported major version: ${distro}"
        esac
        : "${revision:=latest}"
        baseurl="https://dl.rockylinux.org/pub/rocky/${distro#*-}/images/${arch}"
        case "${revision}" in
            latest) revpart=.latest;;
            *)      revpart=-${revision}
        esac
        imgtype=GenericCloud-Base
        baseimagefile="Rocky-${distro#*-}-${imgtype}${revpart}.${arch}.qcow2"
        imageurl="${baseurl}/${baseimagefile}"
        metadatafile=${baseimagefile}.CHECKSUM
        metadatafilepath="${baseimagedir}/${metadatafile}"
        metadataurl="${baseurl}/${metadatafile}"
        check_cmds sha256sum
        verify_download() {
            (cd "${baseimagedir}" && sha256sum -c "${metadatafile}")
        }
        ;;
    explicit/*)
        if [ ! -f "${baseimagedir}/${baseimagefile}" ]; then
            baseimagepath=${baseimagedir}/${baseimagefile}
            error "Base image ${baseimagepath} must exist for ${distro}"
        fi
        ;;
    *) error "Unsupported Linux distribution: ${distro}"
esac
download_and_verify() {
    if [ -n "${metadataurl}" ] && [ ! -f "${metadatafilepath}" ]; then
        if ! curl -Lfso "${metadatafilepath}" "${metadataurl}"; then
            error "Downloading metadata file failed from ${metadataurl}"
        fi
    fi
    if has process_metadata; then
        process_metadata
    fi
    if [ ! -f "${baseimagedir}/${baseimagefile}" ]; then
        if [ -n "${imageurl}" ]; then
            progress "Downloading disk image ${baseimagefile}${compression}"
            file=${baseimagefile}${compression}
            path=${baseimagedir}/${file}
            if ! curl -Lfo "${path}" "${imageurl}"; then
                error "Downloading image failed from ${imageurl}"
            fi
            if [ -n "${compression}" ]; then
                progress "Decompressing disk image ${file} with ${decompress}"
                if ! ${decompress} "${path}"; then
                    error "Failed to decompress ${file}"
                fi
            fi
        else
            error "Disk image ${baseimagefile} not found and unable to download"
        fi
    else
        progress "Disk image ${baseimagefile} found"
    fi
    verification_error=
    if has verify_download && ! verify_download; then
        rm -f "${baseimagedir}/${baseimagefile}" "${metadatafilepath}" \
              "${baseimagedir}/${baseimagefile}${compression}"
        verification_error=yes
    fi
}
download_and_verify
if [ "$verification_error" = yes ]; then
    progress "retry"
    download_and_verify
    if [ "$verification_error" = yes ]; then
        error "Image verification error"
    fi
fi

if [ "${cache_base_image_only}" = "yes" ]; then
    exit 0
fi

check_cmds qemu-img genisoimage ssh-keygen nc mkfifo virsh ssh uuidgen
case "${password}" in
    \$*) ;;
    *) if ! has openssl && ! has mkpasswd; then
        error "neither openssl nor mkpasswd are found, required to run this script"
    fi
esac

if [ -z "${target_env}" ] && [ -n "${TEST_ENV_TARGET}" ]; then
    target_env=$TEST_ENV_TARGET
    progress "Using ${target_env} as the target environment (set by environment variable)"
fi
if [ "${distro}" = coreos ]; then
    provisioning=ignition
    fwcfg_name=opt/com.coreos/config
    : "${username:=core}"
    : "${password:=core}"
fi
: "${num_hosts:?number of hosts must be set with --num-hosts parameter}"
seq 1 "${num_hosts}" >/dev/null 2>/dev/null \
    || error "--num-hosts argument must be a number"
: "${site_name:?site name must be set with --site-name parameter}"
: "${hostname_prefix:=${distro%-*}}"
: "${username:=${distro%-*}}"
: "${password:=${distro%-*}}"
case "${password}" in
    \$*) ;;
    *) if has mkpasswd; then
        password=$(mkpasswd -m sha512crypt "${password}")
    elif has openssl; then
        password=$(openssl passwd -6 "${password}")
    fi
esac
: "${disk_size=4G}"
: "${memory:=2G}"
: "${cpu:=2}"
connected_net() {
    hexnet=$(printf "%02X%02X%02X%02X" 0 "${1:?}" 168 192)
    grep -Eq "^\S*\s+${hexnet}\s+00000000" /proc/net/route
    return $?
}
defined_net() {
    for f in "${netdir}"/*/net"${1:?}".xml; do
        if [ -f "$f" ]; then
            return 0
        fi
    done
    return 1
}
if [ -z "${network}" ]; then
    for net3 in $(seq 171 255); do
        if ! connected_net "${net3}" && ! defined_net "${net3}"; then
            network=192.168.${net3}.0
            break
        fi
    done
    if [ -z "${network}" ]; then
        error "No available subnet found"
    fi
fi
netmask=255.255.255.0
mac_123=22:22:22 ## octets 1-3 of the MAC addr
# shellcheck disable=SC2046,SC2183 # od will produce 2 words
mac_45="$(printf "%s:%s" $(od -An -N2 -tx1 /dev/urandom))"
mac_addr() {
    printf "%s:%s:%02x" "${mac_123}" "${mac_45}" "${1:?}"
}
ip_addr() {
    printf "%s.1%02d" "${network%.*}" "${1:?}"
}

size_to_bytes() {
    case "${1:?}" in
        *T|*t) echo $((${1%[Tt]}*1024*1024*1024*1024));;
        *G|*g) echo $((${1%[Gg]}*1024*1024*1024));;
        *M|*m) echo $((${1%[Mm]}*1024*1024));;
        *K|*k) echo $((${1%[Kk]}*1024));;
        *b)    echo $((${1%b}));;
        *)     echo $((${1})) || echo 0;;
    esac
}
memory_bytes=$(size_to_bytes "${memory}")
[ "${memory_bytes:-0}" -eq 0 ] && error "Invalid memory specification: ${memory}"
[ "${memory_bytes}" -lt $((1 << 29)) ] && error "Memory too low: ${memory}, must be at least 512M"

imagedir="${imagedir}/${site_name}"
domaindir="${domaindir}/${site_name}"
netdir="${netdir}/${site_name}"
sshdir="${sshdir}/${site_name}"
tmpdir="${tmpdir}/${site_name}"
if [ -d "${imagedir}" ]; then
    error "Site ${site_name} already defined, run \`$0 destroy ${site_name}\` first"
fi
mkdir -p "${imagedir}" "${domaindir}" "${netdir}" "${sshdir}" "${tmpdir}"

if [ ! -f "${tmpdir}/host_ed25519_key" ]; then
    progress "Generating SSH host key"
    ssh-keygen -q -t ed25519 -N '' -C '' -f "${tmpdir}/host_ed25519_key" || error "Error creating SSH host key"
fi
if [ ! -f "${sshdir}/id_ed25519" ]; then
    progress "Generating SSH user key"
    ssh-keygen -q -t ed25519 -N '' -C '' -f "${sshdir}/id_ed25519" || error "Error creating SSH user key"
fi

: "${provisioning:=cloud-init}"
case "${provisioning}" in
    cloud-init)
        progress "Generating user-data"
        cat >"${tmpdir}/user-data" <<EOF
#cloud-config
groups:
  - ${username}

user:
  name: ${username}
  primary_group: ${username}
  lock_passwd: false
  passwd: ${password}
  sudo: ALL=(ALL) NOPASSWD:ALL
  shell: /bin/bash
  ssh_authorized_keys:
    - $(cat "${sshdir}/id_ed25519.pub")

growpart:
  mode: auto
  devices: ["/"]

ssh_deletekeys: true
ssh_keys:
  ed25519_private: |$(echo; sed 's/^/    /' "${tmpdir}/host_ed25519_key")
  ed25519_public: $(cat "${tmpdir}/host_ed25519_key.pub")
no_ssh_fingerprints: true

manage_etc_hosts: true

preserve_hostname: false
EOF
        ;;
    ignition)
        cat >"${tmpdir}/config.ign" <<EOF
{
  "ignition": { "version": "3.0.0" },
  "passwd": {
    "users": [
      {
        "name": "${username}",
        "passwordHash": "${password}",
        "sshAuthorizedKeys": [ "$(cat "${sshdir}/id_ed25519.pub")" ]
      }
    ]
  },
  "storage": {
    "files": [{
      "path": "/etc/sudoers",
      "append": [ { "source": "data:;base64,$(echo "${username}	ALL=(ALL) NOPASSWD:ALL" | base64 -w 0)" } ]
    },
    {
      "path": "/etc/ssh/ssh_host_ed25519_key",
      "mode": 384,
      "contents": { "source": "data:;base64,$(base64 -w0 "${tmpdir}/host_ed25519_key")" }
    },
    {
      "path": "/etc/ssh/ssh_host_ed25519_key.pub",
      "mode": 420,
      "contents": { "source": "data:;base64,$(base64 -w0 "${tmpdir}/host_ed25519_key.pub")" }
    }]
  }
}
EOF
        ;;
esac

bridge_id=${network%.*}
bridge_id=${bridge_id#*.}
bridge_id=${bridge_id#*.}
netname="net${bridge_id}"
progress "Creating network ${netname} (${network}/${netmask})"
cat >"${netdir}/${netname}.xml" <<EOF
<network>
  <name>${netname}</name>
  <uuid>$(uuidgen)</uuid>
  <forward mode='nat'/>
  <bridge name='virbr${bridge_id}' stp='on' delay='0'/>
  <ip address='${network%.*}.1' netmask='${netmask}'>
    <dhcp>
      <range start='${network%.*}.200' end='${network%.*}.250'/>
EOF
for n in $(seq 1 "${num_hosts}"); do
    cat >>"${netdir}/${netname}.xml" <<EOF
      <host mac="$(mac_addr "$n")" name="$(name "$n")" ip="$(ip_addr "$n")"/>
EOF
done
cat >>"${netdir}/${netname}.xml" <<EOF
    </dhcp>
  </ip>
</network>
EOF
virsh net-define "${netdir}/${netname}.xml" || error "Error creating network ${netname}"
virsh net-start "${netname}" || error "Error starting network ${netname}"

progress "Creating SSH config"
cat >"${sshdir}/config" <<EOF
IdentityFile ${sshdir}/id_ed25519
IdentitiesOnly yes
UserKnownHostsFile=${sshdir}/known_hosts
${EXTRA_SSH_CONFIG}
EOF
: >"${sshdir}/known_hosts"
for n in $(seq 1 "${num_hosts}"); do
    cat >>"${sshdir}/config" <<EOF
Host $(name "$n")
    User ${username}
    HostName $(ip_addr "$n")
EOF
    (ip_addr "$n"; printf " "; cat "${tmpdir}/host_ed25519_key.pub") >> "${sshdir}/known_hosts"
done

progress "Creating virtual machines"
for n in $(seq 1 "${num_hosts}"); do
    name="$(name "$n")"
    base_disk_size=$(qemu-img info "${baseimagedir}/${baseimagefile}" | \
                  grep 'virtual size' | sed 's/.*(\([0-9]*\) bytes).*/\1/')
    disk_size_bytes=$(size_to_bytes "${disk_size}")
    [ "${disk_size_bytes:-0}" = 0 ] && error "Invalid disk size specification: ${disk_size}"
    ## layer virtual size should not be smaller than the base image virtual size
    if [ "${disk_size_bytes}" -lt "${base_disk_size}" ]; then
        disk_size=${base_disk_size}b
    fi
    qemu-img create -f qcow2 -b "${baseimagedir}/${baseimagefile}" \
        -F qcow2 "${imagedir}/${name}.qcow2" "${disk_size}" \
        || error "Error creating qcow2 file for ${name}"
    provisioning_device=
    provisioning_config=
    case "${provisioning}" in
        cloud-init)
            mkdir -p "${tmpdir}/${name}"
            cat >"${tmpdir}/${name}/meta-data" <<EOF
instance-id: ${name}
dsmode: local
local-hostname: ${name}
EOF
            rm -f "${imagedir}/${name}.init.iso"
            genisoimage -output "${imagedir}/${name}.init.iso" \
                -volid cidata -joliet -rock \
                "${tmpdir}/user-data" "${tmpdir}/${name}/meta-data" \
                || error "Error generating cloud-init iso for ${name}"
            provisioning_device=$(cat <<EOF
    <disk type="file" device="disk">
      <driver name="qemu" type="raw"/>
      <source file="${imagedir}/${name}.init.iso"/>
      <target dev="vdc" bus="virtio"/>
      <readonly/>
    </disk>
EOF
            )
        ;;
        ignition)
            provisioning_config=$(cat <<EOF
  <sysinfo type="fwcfg">
    <entry name="${fwcfg_name}" file="${tmpdir}/config.ign"/>
  </sysinfo>
EOF
            )
        ;;
    esac
    extra_devices="$(eval echo "\$extra_device_${n}")"
    case "${arch}" in
        x86_64) qemu_machine=q35;;
        aarch64) qemu_machine=virt;;
    esac
    cat >"${domaindir}/${name}.xml" <<EOF
<domain type="kvm">
  <name>${site_name}-${name}</name>
  <uuid>$(uuidgen)</uuid>
  <memory unit="bytes">${memory_bytes}</memory>
  <currentMemory unit="bytes">${memory_bytes}</currentMemory>
  <vcpu>${cpu}</vcpu>
  <os firmware='efi'>
    <type arch='${arch}' machine='${qemu_machine}'>hvm</type>
    <boot dev="hd"/>
    <firmware>
      <feature enabled='no' name='secure-boot'/>
    </firmware>
  </os>
  <cpu mode="host-passthrough"/>
  <clock offset="utc">
    <timer name="kvmclock"/>
  </clock>
  <features>
    <acpi/>
    <apic/>
  </features>
  ${provisioning_config}
  <devices>
    <emulator>/usr/bin/qemu-system-${arch}</emulator>
    <disk type="file" device="disk">
      <driver name="qemu" type="qcow2"/>
      <source file="${imagedir}/${name}.qcow2"/>
      <backingStore type="file">
        <format type="qcow2"/>
        <source file="${baseimagedir}/${baseimagefile}"/>
      </backingStore>
      <target dev="vda" bus="virtio"/>
    </disk>
    ${provisioning_device}
    <interface type="network">
      <source network="${netname}"/>
      <mac address="$(mac_addr "$n")"/>
      <model type="virtio"/>
    </interface>
    <memballoon model='virtio' freePageReporting='on'/>
    <console type="pty"/>
    ${extra_devices}
  </devices>
</domain>
EOF
    virsh define "${domaindir}/${name}.xml" || error "Error creating domain ${site_name}-${name}"
    virsh start "${site_name}-${name}" || error "Error starting domain ${site_name}-${name}"
done

progress "Waiting for the hosts to come up"
for n in $(seq 1 "${num_hosts}"); do
    while ! nc -z "$(ip_addr "$n")" 22; do sleep 0.1; done
done
progress "Waiting for SSH"
for n in $(seq 1 "${num_hosts}"); do
    while ! ssh -n -T -F "${sshdir}/config" "$(name "$n")"; do sleep 0.5; done
done

if [ -n "${no_install}" ]; then
    exit 0
fi
progress
progress "Preparing to install supd"

if [ -n "${target_env}" ]; then
    environment=${target_env}
    echo "Using environment ${environment}"
elif has supctl; then
    if ! supctl 'do' token-info >/dev/null; then
        progress 'Not installing supd: supctl found but not logged in'
        exit 0
    fi
    environment=$(supctl profile show | sed -En 's/Host: (api.)?//p')
    if ! tty >/dev/null; then
        error 'Not installing supd: --target-env must be set when running non-interactively'
    else
        printf "Using environment %s, please confirm" "${environment}"
        if ! read -r _continue; then
            progress 'Not installing supd: environment not confirmed'
            exit 0
        fi
    fi
else
    progress 'Not installing supd: supctl not found'
    exit 0
fi

progress "Installing supd"
pids=
pipes=
for n in $(seq 1 "${num_hosts}"); do
    name="$(name "$n")"
    pipe_name=${tmpdir}/supd-install-${name}.pipe
    pipes="${pipes} ${pipe_name}"
    mkfifo "${pipe_name}"
    ssh -F "${sshdir}/config" "${name}" \
        "curl https://api.${environment}/install | sudo sh -s -- ${installer_flags} -y" \
         >"${pipe_name}" 2>&1 &
    pids="$pids $!"
    sed "s/^/${name}: /" <"${pipe_name}" &
done
nonzero_exit=
for pid in $pids; do
    if ! wait "${pid}"; then
        nonzero_exit=yes
    fi
done
wait
for pipe in ${pipes}; do
    rm -f "${pipe}"
done
if [ "${nonzero_exit}" = yes ]; then
    error 'One or more supd installations failed'
fi

site_yaml=$(cat <<EOF
name: ${site_name}
type: edge
topology:
  parent-site:
    control-tower
EOF
if [ -n "${site_labels}" ]; then
    echo "labels:"
    echo "${site_labels}" | \
        tr "\t" "\n" | \
        sed -e '/^$/d' \
            -e '/^[^=]\+$/{s/$/: []/}' \
            -e 's/=/: /' \
            -e 's/^/  /'
fi
cat <<EOF
ingress-allocation-method: pool
ingress-ipv4-address-ranges:
  - range: ${network%.*}.2-${network%.*}.99
    network-prefix-length: 24
hosts:
EOF
)
for n in $(seq 1 "${num_hosts}"); do
    name="$(name "$n")"
    host_id=$(ssh -F "${sshdir}/config" "${name}" "sed -n 's/Using host id \([0-9a-f-]\+\)/\1/p' /tmp/install.log")
    if [ -n "${host_id}" ]; then
        site_yaml=$(cat <<EOF
${site_yaml}
  - host-id: ${host_id}
    labels:
      hostname: ${name}
EOF
)
    else
        error "Error fetching host-id for ${name}"
    fi
done
if [ -n "${location}" ] && has jq; then
    if ! [ -f "${location_cache}" ]; then
        echo '[]' > "${location_cache}"
    fi
    location_entry=$(jq -c '.[] | select(.description=="'"${location}"'")' <"${location_cache}")
    if [ -z "${location_entry}" ]; then
        progress "Fetching location data for ${location} using OpenStreetMap API"
        geocodeurl="https://nominatim.openstreetmap.org/search"
        location_entry=$(curl -fs --get \
                             --data-urlencode "format=jsonv2" \
                             --data-urlencode "limit=1" \
                             --data-urlencode "q=${location}" \
                             "${geocodeurl}" \
                             | jq -c '{"description": "'"${location}"'",
                                        "latitude": .[0].lat,
                                        "longitude": .[0].lon,
                                       } | map_values(. // empty)')
        jq ". += [${location_entry}]" <"${location_cache}" >"${location_cache}.new" && \
            mv "${location_cache}.new" "${location_cache}"
    else
        progress "Using cache location data for ${location}"
    fi
    site_yaml=$(cat <<EOF
${site_yaml}
location: ${location_entry}
EOF
)
fi

printf "\nCreate site %s with the following configuration\n\n%s\n\n" "${site_name}" "${site_yaml}"

no_create_reason=
if has supctl; then
    if ! supctl 'do' token-info >/dev/null; then
        no_create_reason="supctl found, but not logged in"
    else
        environment=$(supctl profile show | sed -En 's/Host: (api.)?//p')
        if [ -n "${target_env}" ]; then
            if [ "${target_env}" != "${environment}" ]; then
                no_create_reason=$(cat <<EOF
discrepancy
    - supctl is logged into: ${environment},
    - target environment: ${target_env}
EOF
)
            fi
        else
            if ! tty >/dev/null; then
                no_create_reason='--target-env must be set when running non-interactively'
            else
                printf "Using environment %s, please confirm" "${environment}"
                if ! read -r _continue; then
                    no_create_reason='environment not confirmed'
                fi
            fi
        fi
    fi
else
    no_create_reason="supctl not found"
fi
if [ -z "${no_create_reason}" ]; then
    progress "Creating site ${site_name} in environment ${environment}"
    progress "supctl create system sites ${site_name}"
    echo "${site_yaml}" | supctl create system sites "${site_name}"
else
    progress "Not writing configuration for site ${site_name} due to: ${no_create_reason}"
    progress 'Please create the site manually using the above configuration'
fi
