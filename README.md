# test-env
Quickly spin up a set of VMs and configure as an edge site in ACP environment.

The script downloads a base image based on the distro specified from
the distribution maintainer's website and verifies the checksum. Then
it creates a libvirt domain xml definition and a cloud-init file corresponding
to other parameters. When the VMs are started in runs the supd installation
and creates the site definition in ACP using the `supctl create system sites`
command. The script is interactive by default and asks permission before
executing commands towards the ACP environment.

## Requirements
- `libvirt` daemon installed and running, `qemu:///system` is accessible by
  the current linux user
  - this may be achieved by the user being a member of libvirt group
- `libvirt-qemu` user has access to the current directory
- the following executables are available in `$PATH`:
    - `qemu-img`
    - `genisoimage`
    - `ssh-keygen`
    - `openssl`
    - `nc`
    - `curl`
    - `virsh`
    - `ssh`
    - `jq` (for Fedora CoreOS and for adding location information)
    - `sha256sum` (for Ubuntu image checksum verification)
    - `sha512sum` (for Debian image checksum verification)
    - `supctl` (optional, to create the site config in the target environment
      automatically)
- `supctl` default profile is logged into the target ACP environment with
  site provider credentials in order for site configuration to be written
  to the target environment automatically (optional)

## Example

The following command creates 3 Debian 12 VMs and creates a corresponding site
called `test` in ACP.
```
./test-env.sh create --num-hosts 3 --site-name test --distro debian-12
```

The above command is identical to the following:
```
./test-env.sh create --profile debian
```

To see the available profiles and their definitions see `./test-env.cfg`.

A second site can be started and be running simultaneously with the first.
It must have a different site name.
```
./test-env.sh create --num-hosts 3 --site-name test2 --distro ubuntu-22.04
```

To ssh into machine `debian01` on site `test` use the following command:
```
ssh -F ssh/test/config debian01
```

When a site is no longer needed it can be destroyed with:
```
./test-env.sh destroy test --delete
```

To destroy all sites:
```
./test-env.sh destroy --delete
```

The `--delete` flag is optional and indicates that the site definition should
be deleted from the ACP environment.

## Details

To see all available command line parameters run:
```
./test-env.sh --help
```
